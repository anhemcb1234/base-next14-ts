import { useState, useEffect } from 'react';

import { UseDebounceOptions } from '@/types/hook/hookTypes';

const useDebounce = <T>({ value, delay = 600 }: UseDebounceOptions<T>): T => {
  const [debouncedValue, setDebouncedValue] = useState<T>(value);

  useEffect(() => {
    // Update debounced value after delay
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    // Cleanup function to clear timeout if the value changes before the delay
    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debouncedValue;
};

export default useDebounce;
