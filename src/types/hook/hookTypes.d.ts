export type UseDebounceOptions<T> = {
  value: T;
  delay: number;
};
