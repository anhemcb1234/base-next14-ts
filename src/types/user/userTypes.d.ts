export type UserData = {
  id: number;
  name: string;
  age: number;
};

export type UserFormFormData = {
  username: string;
  password: string;
};

export type UserStore = {
  firstName: string;
  lastName: string;
  setFirstName: (string: string) => void;
  setLastName: (string: string) => void;
};

export interface User {
  id: number;
  fullName: string;
  // Add other fields as necessary
}
