/** Common Server Response Type */
export type ServerResponse<T> = Promise<{
  data: {
    /** API Response Status Code */
    code: number;
    /** API Response Status Message */
    statusText: string;
    /** API Response Data */
    content: T;
  };
}>;

/** Common Server Id Response Type */
export type ServerCommonIdResponse = ServerResponse<{ id: number }>;
