import Axios from '@/services/Axios';

import type { MockupPokemonRequest } from '../types/user.dto';

const getPokemon = (payload?: MockupPokemonRequest) => {
  const limit = payload?.limit ?? 10;
  const page = payload?.page ?? 1;
  const offset = page * limit;
  return Axios.getRequest(`/pokemon/`, { offset, limit });
};

const userServices = {
  getPokemon,
};

export default userServices;
