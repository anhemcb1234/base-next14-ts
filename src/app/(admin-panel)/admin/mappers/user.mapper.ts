import { UserDTO } from '../types/user.dto';

const toDTO = (data: any): UserDTO | UserDTO[] => {
  if (Array.isArray(data)) {
    return data.map((item) => ({
      id: item.url,
      name: item.name,
      email: item.url,
    })) as UserDTO[];
  }
  return {
    id: data.id,
    name: data.name,
    email: data.email,
  } as UserDTO;
};

const fromDTO = (dto: UserDTO): UserDTO => ({
  id: dto.id,
  name: dto.name,
  email: dto.email,
});

export const UserMapper = {
  toDTO,
  fromDTO,
};
