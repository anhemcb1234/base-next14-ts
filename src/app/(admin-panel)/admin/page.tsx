'use client';

import dataHandler from './dataHanler';
import Table from './components/Table';

const AdminPage = () => {
  const { data } = dataHandler();

  return (
    <section>
      HTT Admin
      <div className=''>
        <Table />
      </div>
    </section>
  );
};

export default AdminPage;
