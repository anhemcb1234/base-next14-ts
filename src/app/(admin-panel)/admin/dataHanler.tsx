import useSWR from 'swr';

import type { UserDTO } from './types/user.dto';

import userServices from './services/userServices';
import { UserMapper } from './mappers/user.mapper';

const dataHandler = () => {
  const { data } = useSWR<UserDTO[], Error>(`getListGrade`, async () => {
    const response = await userServices.getPokemon({
      limit: 10,
      page: 0,
    });
    const { results } = response.data;
    console.log('123123', UserMapper.toDTO(results))
    return UserMapper.toDTO(results) as UserDTO[];
  });

  return {
    data,
  };
};
export default dataHandler;
