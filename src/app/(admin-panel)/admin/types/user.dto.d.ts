export type UserDTO = {
  id: number;
  name: string;
  email: string;
};

export type MockupPokemonRequest = {
  page: number;
  size?: number;
  limit?: number;
};
