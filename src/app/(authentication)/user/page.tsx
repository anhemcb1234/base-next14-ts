import Link from 'next/link';

import Card from '@/components/card/Card';
import userServices from '@/services/user/userServices';

export default async function UserPage() {
  const article = await userServices.getListUser({
    page: 0,
    size: 10,
  });

  return (
    <main className=''>
      Server
      <Link href='user/create-user'>Create User</Link>
      <Card initialData={article.data} />
    </main>
  );
}
