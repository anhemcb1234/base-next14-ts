import { NextRequest, NextResponse } from 'next/server';
import { tokenMiddleware } from '@/middleware/tokenMiddleware';

const PUBLIC_FILE = /\.(.*)$/;

export async function middleware(req: NextRequest) {
  // const tokenResponse = tokenMiddleware(req);
  // if (tokenResponse instanceof NextResponse) {
  //   return tokenResponse;
  // }

  // if (
  //   req.nextUrl.pathname.startsWith('/_next') ||
  //   req.nextUrl.pathname.includes('/api/') ||
  //   PUBLIC_FILE.test(req.nextUrl.pathname)
  // ) {
  //   return;
  // }

  return NextResponse.next();
}

export const config = {
  matcher: ['/admin/:path*'], // Apply to routes starting with /admin/ and /system-admin/
};
