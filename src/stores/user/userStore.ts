import { create } from 'zustand';
import { UserStore } from '@/types/user/userTypes';

const useUserStore = create<UserStore>((set) => ({
  firstName: '123123',
  lastName: '',
  setFirstName: (string: string) => set({ firstName: string }),
  setLastName: (string: string) => set({ lastName: string }),
}));

export default useUserStore;
