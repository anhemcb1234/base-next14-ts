import { create } from 'zustand';

type MockupData = {
  mockArr: string[];
  setMockArr: (arr: string[]) => void;
};

export const useMockupStore = create<MockupData>((set) => ({
  mockArr: [],
  setMockArr: (arr) => set({ mockArr: arr }),
}));
