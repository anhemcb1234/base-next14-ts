import { NextRequest, NextResponse } from 'next/server';
import { getCookie } from 'cookies-next';
import { cookies } from 'next/headers';

export const tokenMiddleware = (request: NextRequest) => {
  // Get the token from the request header
  const token = getCookie('token', { cookies });

  // Verify the token
  if (!token) {
    // If the token is invalid, return a 401 Unauthorized response
    return NextResponse.redirect(new URL('/', request.url));
  }

  // If the token is valid, allow the request to proceed
  return NextResponse.next();
  // return NextResponse.redirect(new URL('/', request.url));
};
