import axios, {
  AxiosResponse,
  InternalAxiosRequestConfig,
  RawAxiosRequestHeaders,
  type AxiosInstance,
  type AxiosRequestConfig,
} from 'axios';

const axiosDefaultHeaderOption = <const>{
  'cache-control': 'public, s-maxage=10, stale-while-revalidate=59',
};

const axiosInstance: AxiosInstance = axios.create({
  baseURL: process.env.NEXT_PUBLIC_SERVER_URL,
  headers: axiosDefaultHeaderOption,
});

axiosInstance.interceptors.request.use((config: InternalAxiosRequestConfig<unknown>) => {
  // const token = localStorage.getItem("token");
  const token =
    'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwidXNlcm5hbWUiOiJhZG1pbiIsInJvbGUiOiJST0xFX0FETUlOIiwiZXhwIjoxNzEzNDExNTkyLCJpYXQiOjE3MTMzMjUxOTJ9.Qpq5I36Jyx9XlU_Uh2-VmExf1QKnk3SMJSp1tgvhSRyITa8h93pFGUBo5HxF2onBiVWQemMiOKwH9J9GOIx7_w';
  if (token) {
    config.headers = config.headers ?? {}; // Add this line to ensure headers is not undefined
    config.headers['x-api-token'] = token;
    config.headers['Authorization'] = `Bearer ${token}`;
  }
  return config;
});

axiosInstance.interceptors.response.use(
  (response: AxiosResponse) => response,
  (error) => {
    console.log(error);
    if (error?.response?.data?.code === 1009 || error?.response?.data?.code === 1007) {
      localStorage.removeItem('token');
      window.location.href = '/';
    }
    return Promise.reject(error);
  },
);

export const getAPIHeaders = () => axiosInstance.defaults.headers.common;

export const setAPIHeaders = (inputHeaders: RawAxiosRequestHeaders) =>
  (axiosInstance.defaults.headers.common = {
    ...axiosInstance.defaults.headers.common,
    ...inputHeaders,
  });

export const getRequest = (
  endpointApiUrl: string,
  payload: Record<string, unknown> = {},
  config: AxiosRequestConfig = {},
): Promise<AxiosResponse> =>
  axiosInstance.get(endpointApiUrl, {
    params: payload,
    ...config,
  });

export const postRequest = (
  endpointApiUrl: string,
  payload: Record<string, unknown> = {},
  config: AxiosRequestConfig = {},
): Promise<AxiosResponse> => axiosInstance.post(endpointApiUrl, payload, config);

export const putRequest = (
  endpointApiUrl: string,
  payload: Record<string, unknown> = {},
  config: AxiosRequestConfig = {},
): Promise<AxiosResponse> => axiosInstance.put(endpointApiUrl, payload, config);

export const patchRequest = (
  endpointApiUrl: string,
  payload: Record<string, unknown> = {},
  config: AxiosRequestConfig = {},
): Promise<AxiosResponse> => axiosInstance.patch(endpointApiUrl, payload, config);

export const deleteRequest = (endpointApiUrl: string, config: AxiosRequestConfig = {}): Promise<AxiosResponse> =>
  axiosInstance.delete(endpointApiUrl, config);

const Axios = {
  getRequest,
  postRequest,
  putRequest,
  patchRequest,
  deleteRequest,
};

export default Axios;
