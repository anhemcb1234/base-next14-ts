'use client';

import React, { useState, FormEvent } from 'react';
import { useRouter } from 'next/navigation';

import { UserFormFormData } from '@/types/user/userTypes';
import useUserStore from '@/stores/user/userStore';

const UserForm = () => {
  const router = useRouter();

  const { firstName, setFirstName } = useUserStore();

  const [formData, setFormData] = useState<UserFormFormData>({
    username: '',
    password: '',
  });

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    // Do something with the form data, like submitting it to a server
    console.log('Form submitted:', formData);
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handleGoBack = () => {
    window.history.back();
  };

  <button onClick={handleGoBack}>Go Back</button>;
  const handleSetLastName = () => {
    setFirstName('hoang tien');
  };
  return (
    <>
      <button onClick={handleSetLastName}>Submit</button>
      <form onSubmit={handleSubmit}>
        <label>
          Username: {firstName}
          <input type='text' name='username' value={formData.username} onChange={handleChange} />
        </label>
        <br />
        <label>
          Password:
          <input type='password' name='password' value={formData.password} onChange={handleChange} />
        </label>
        <br />
        <button type='submit'>Submit</button>
      </form>
      <button type='button' onClick={() => router.back()}>
        Go back
      </button>
    </>
  );
};

export default UserForm;
