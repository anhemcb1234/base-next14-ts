# Wizcore-Frontend

_this development guide based on CY's front-end coding conventions_

> **Note**: The coding conventions in this README are adapted in part from [airbnb's](https://github.com/airbnb/javascript) coding conventions.

## Table of Contents

1. [Project Start](#project-start)
2. [Project Structure](#project-structure)
3. [File Naming Convention](#file-naming-convention)
4. [Code Naming Convention](#code-naming-convention)
5. [Coding Convention](#coding-convention)

## Project Start

<a name="projectStart--installation"></a>

[1.1](#projectStart--installation) **Installation**: To install project dependencies, run the following command

```bash
$ pnpm install
```

<a name="projectStart--runningTheProject"></a>

[1.2](#projectStart--runningTheProject) **Running the Project**: To start the development server, execute

```bash
$ pnpm run dev
```

<a name="projectStart--runningUtilScripts"></a>

[1.3](#projectStart--runningUtilScripts) **Running Util Scripts**: excute installed library provides utilities like formatting and code checks

```bash
$ pnpm run lint
$ pnpm run format
$ pnpm run type-check
```

## Project Structure

The folder structure of a project follows the default settings of the framework used for each project first, and then refines them based on the project's domain.

- `/public`: Folder for managing asset files of the project.
- `/src`: Main folder for managing the project's source code.
  - `/src/app`: Manages page components based on app routing.
  - `/src/components`: Manages components.
  - `/src/hook`: Stored of hook.
  - `/src/services`: Contains API functions.
  - `/src/stores`: Manages zustand stores.
  - `/src/style`: Manages style css.
  - `/src/types`: Manages TypeScript types.
  - `/src/utils`: Contains utility functions for convenience.
  - `/src/middleware`: Contains middleware functions for handling requests and responses.
  - `/src/plugin`: Contains plugins to extend the functionality of the application.

## File Naming Convention

- [3-1]

## Code Naming Convention

<a name="codeNamingConvention--camelCase"></a>

[4-1](#codeNamingConvention--camelCase) Use camelCase when naming objects, functions, and instances

```typescript
// bad
const OBJEcttsssss = {};
const this_is_my_object = {};
function c() {}

// good
const thisIsMyObject = {};
function thisIsMyFunction() {}
```

<a name="codeNamingConvention--PascalCase"></a>

[4-2](#codeNamingConvention--PascalCase) Use PascalCase when naming classes, constructors, types, and interfaces

```typescript
// bad
function user(options) {
  this.name = options.name;
}

const bad = new user({
  name: 'nope',
});

// good
class User {
  constructor(options) {
    this.name = options.name;
  }
}

const good = new User({
  name: 'yup',
});

// bad
type user = {
  name: string;
};

interface team {
  users: user[];
}

// good
type User = {
  name: string;
};

interface Team {
  users: User[];
}
```

<a name="codeNamingConvention-- "></a>

[4-3](#) Do not use trailing or leading underscores

> Why? JavaScript does not have the concept of privacy in terms of properties or methods. Although a leading underscore is a common convention to mean “private”, in fact, these properties are fully public, and as such, are part of your public API contract. This convention might lead developers to wrongly think that a change won’t count as breaking, or that tests aren’t needed. tl;dr: if you want something to be “private”, it must not be observably present.

```javascript
// bad
this.__firstName__ = 'Panda';
this.firstName_ = 'Panda';
this._firstName = 'Panda';

// good
this.firstName = 'Panda';

// good, in environments where WeakMaps are available
// see https://compat-table.github.io/compat-table/es6/#test-WeakMap
const firstNames = new WeakMap();
firstNames.set(this, 'Panda');
```

<a name="codeNamingConvention-- "></a>

[4-4](#)

<a name="codeNamingConvention-- "></a>

[4-5]()

<a name="codeNamingConvention-- "></a>

[4-6](#)

## Coding Convention
